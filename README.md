
# PriseNote

PriseNote est une application de prise de note avec la syntaxe asciidoctor .

## Asciidoctor
AsciiDoc est un format de document texte. Il peut être utilisé pour écrire de la documentation, des livres, des pages Web, des pages de manuel et bien d’autres.

Comme il est très configurable, les documents AsciiDoc peuvent être convertis dans de nombreux autres formats tels que HTML, PDF, pages de manuel, EPUB, etc.

Comme la syntaxe AsciiDoc est assez basique, elle est devenue très populaire avec un support important dans divers plugins de navigateur, plugins pour langages de programmation et autres outils.

Pour en savoir plus sur l’outil, nous vous suggérons de lire la official documentation où vous pouvez trouver de nombreuses ressources utiles pour apprendre la syntaxe et les méthodes permettant d’exporter votre document AsciiDoc dans un autre format

Asciidoctor est une chaîne de traitement de texte et de publication rapide permettant de convertir le contenu AsciiDoc au format HTML5, DocBook 5, EPUB3, PDF et autres formats. Asciidoctor est l'implémentation principale de la syntaxe AsciiDoc, d'abord introduite et implémentée dans le projet AsciiDoc basé sur Python.

Asciidoctor est écrit en Ruby et peut être utilisé sur n'importe quel runtime Ruby, y compris JRuby. Il peut également être exécuté sur la machine virtuelle via AsciidoctorJ, une API Java formelle exploitant JRuby, ou exécuté en JavaScript avec Asciidoctor.js, une version transcompilée de la base de code Ruby.

Asciidoctor est open source, disponible sous licence MIT. Asciidoctor RubyGem est publié sur rubygems.org et peut également être installé sur plusieurs distributions Linux populaires à l'aide de l'un des packages fournis. Les référentiels git du projet sont hébergés sous l' organisation Asciidoctor sur GitHub afin d'encourager la participation la plus large possible.

Bien qu'Asciidoctor vise à offrir une conformité totale avec la syntaxe AsciiDoc mise en œuvre par AsciiDoc Python ( à quelques exceptions près ), il s'agit bien plus que d'un clone d'AsciiDoc Python.



## Objectif de l'application
PriseNote est une  application permettant d’éditer et d’organiser des notes . Par exemple, elle  permet la prise de notes durant une réunion. Les notes seront rédigées par l’utilisateur en utilisant le langage de balisage léger asciidoctor1. Elle a pour but de faciliter la creation et la recherche de notes , aussi la conversion des notes en formats pdf, html etc.



## Aspects techniques
aspects techniques ; outils utilisés pour la réalisation de l'aplication
l'implémentation

### Les outils et langages utilisés


#### Maven
Maven, signifiant « accumulateur de connaissances» , a été conçu comme une tentative de simplification des processus de construction dans le projet Jakarta Turbine. Il y avait plusieurs projets, chacun avec leurs propres fichiers de construction Ant, tous légèrement différents. Les fichiers JAR ont été enregistrés dans CV.c'etait un moyen standard de construire les projets, une définition claire de son contenu, un moyen facile de publier des informations sur le projet et un moyen de partager les fichiers JAR entre plusieurs projets.

Le résultat est un outil qui peut maintenant être utilisé pour créer et gérer tout projet basé sur Java. 

##### Objectifs de Maven
L'objectif principal de Maven est de permettre à un développeur de comprendre l'état complet d'un effort de développement dans les meilleurs délais. Afin d'atteindre cet objectif, Maven tente de traiter plusieurs domaines de préoccupation:

Rendre le processus de construction facile
Fournir un système de construction uniforme
Fournir des informations de qualité sur les projets
Fournir des directives pour le développement de meilleures pratiques
Permettre une migration transparente vers de nouvelles fonctionnalités

#### Eclipse
Eclipse est un projet, décliné et organisé en un ensemble de sous-projets de développements logiciels, de la fondation Eclipse visant à développer un environnement de production de logiciels libre qui soit extensible, universel et polyvalent, en s'appuyant principalement sur Java.

Son objectif est de produire et fournir des outils pour la réalisation de logiciels, englobant les activités de programmation (notamment environnement de développement intégré et frameworks) mais aussi d'AGL recouvrant modélisation, conception, test, gestion de configuration, reporting… Son EDI, partie intégrante du projet, vise notamment à supporter tout langage de programmation à l'instar de Microsoft Visual Studio.

Bien qu'Eclipse ait d'abord été conçu uniquement pour produire des environnements de développement, les utilisateurs et contributeurs se sont rapidement mis à réutiliser ses briques logicielles pour des applications clientes classiques. Cela a conduit à une extension du périmètre initial d'Eclipse à toute production de logiciel : c'est l'apparition du framework Eclipse RCP en 2004.

Figurant parmi les grandes réussites de l'open source, Eclipse est devenu un standard du marché des logiciels de développement, intégré par de grands éditeurs logiciels et sociétés de services. Les logiciels commerciaux Lotus Notes 8, IBM Lotus Symphony ou WebSphere Studio Application Developer sont notamment basés sur Eclipse.


#### java
Java est un langage de programmation orienté objet créé par James Gosling et Patrick Naughton, employés de Sun Microsystems, avec le soutien de Bill Joy (cofondateur de Sun Microsystems en 1982), présenté officiellement le 23 mai 1995 au SunWorld.

La société Sun a été ensuite rachetée en 2009 par la société Oracle qui détient et maintient désormais Java.

La particularité et l'objectif central de Java est que les logiciels écrits dans ce langage doivent être très facilement portables sur plusieurs systèmes d’exploitation tels que Unix, Windows, Mac OS ou GNU/Linux, avec peu ou pas de modifications, mais qui ont l'inconvénient d'être plus lourd à l'exécution (en mémoire et en temps processeur) à cause de sa machine virtuelle. Pour cela, divers plateformes et frameworks associés visent à guider, sinon garantir, cette portabilité des applications développées en Java.

### les classes principales
```bash
App
ConfigMethod
CreerNoteCommand
EditNoteCommand
GenerateIndexCommand
I_Commande
ListerNoteCommand
Note
SearchNoteCommand
SupNoteCommand
SupNoteCommandArg
VoirNoteCommand
VoirNoteCommandArg


```
```bash
I_Commande
```
C'est une interface avec une methode execute ,nous allons définir pour chaque commande une classe qui va implementer cette interface (implémentation du pattern [Command](https://dzone.com/articles/design-patterns-command)).
```bash
CreerNoteCommand
```
Cette classe implémente l'interface I_Commande et permet la création de notes avec et sans argumennts.
```bash
EditNoteCommand
```
Cette classe implémente l'interface I_Commande et permet la modification et la création de notes avec et sans argumennts. Si la note existe, elle permet de l'ouvrir avec un éditeur pour la modifier sinon si la note n'existe pas , elle créé la note et ouvre l'éditeur pour que l'utilisateur modifie le contenu de la note créée.
```bash
ListerNoteCommand
```
Cette classe implémente l'interface I_Commande et permet de lister toutes les  notes disponibles.
```bash
SupNoteCommand
```
Cette classe implémente l'interface I_Commande et permet la suppression de notes  sans argumennts.
```bash
SupNoteCommandArg
```
Cette classe implémente l'interface I_Commande et permet la suppression de notes  argumennts.

```bash
VoirNoteCommand
```
Cette classe implémente l'interface I_Commande et permet l'ouverture d'une note sans argumennts dans le navigateur.
```bash
VoirNoteCommandArg
```
Cette classe implémente l'interface I_Commande et permet l'ouverture d'note avec  argumennts dans le navigateur.
```bash
SearchNoteCommand
```
Cette classe implémente l'interface I_Commande et permet la recherche de notes par mots clés.
```bash
GenerateIndexCommand
```
Cette classe implémente l'interface I_Commande et permet la génération d'un fichier index contenant les notes existan

```bash
ConfigMethod
```
C'est la classe dans laquelle on a créé le fichier de configuration, c'est ce fichier qui va permettre à l'utilisateur de modifier le repertoire d'enregistrement de ces notes par defaut, l'editeur avec lequel il ouvre ces notes etc.
```bash
App
```
C'est la classe principale dans laquelle on appelle toutes les commandes implémentées.

### Manuel Utilisateur
L'application PriseNote a un certain nombre de fonctionnalités entre autre la création, la suppression,la modification de notes.
L'utilisateur pour pouvoir acceder à ses fonction dois installer l'application , et taper les commmandes 
adéquats pour une opération donnée, dans la section suivante nous présentons les différentes commandes 
par rapport aux fonctions associées.
L'utilisateur dispose également d'un fichier conf.txt dans lequel il peut modifier le repertoire d'enregistrement de ces notes par defaut, l'editeur avec lequel il ouvre ces notes etc.
L'utilisateur a le choix d'exécuter ces différentes commandes sans argument ou avec argument.
Exemple:
```bash
create Note 
```
```bash
create 
```
Cependant l'utilisateur doit respecter la syntaxe asciidoctor pour la saisie de ses notes , voir [SyntaxAsciidoctor](https://asciidoctor.org/docs/user-manual/).
Exemple:
```bash
= Réunion CDS Stéphane Lopes <stephane.lopes@uvsq.fr> 
22/02/2018 
:context: work
:project: cds
* Edgar Allen Poe
* Sheri S. Tepper
* Bill Bryson
 ```
#### Les fonctionnalités de l'application et les commandes associées

```
	   
            'fonctionnalités'                                    'commandes'

            creation de notes ou modifications de notes          create \ edit
   
	        Affichage des notes existantes                       lister \ ls
         
            Suppression d'une note                               delete \ del
   
            voir une note dans le navigateur                      view \ v

            Rechercher une note                                   search \ s
	        
    
```


#### Compilation du projet
```bash
mvn package
```

#### Lancement de l'application
```bash
java -jar .\target\asciidoctor-maven-plugin-2.0.0-SNAPSHOT.jar
```

## License
[GPL](https://fr.wikipedia.org/wiki/Licence_publique_g%C3%A9n%C3%A9rale_GNU)