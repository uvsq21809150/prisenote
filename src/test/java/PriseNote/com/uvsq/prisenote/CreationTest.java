/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package PriseNote.com.uvsq.prisenote;

import org.junit.Test;

import Config.ConfigMethod;

import static org.junit.Assert.*;

/**
 *
 * @author mayim
 */
public class CreationTest {
    
     @Test
    public void Test1() 
    {
        String titre = "ma_Note";
    	 String contenu = 
    			 " = ma_Note"
    			 + "Stéphane Lopes <stephane.lopes@uvsq.fr>\n" + 
    			 "22/02/2018\n" + 
    			 ":context: Test_Context\n" + 
    			 ":project: Test_Project"
    			 + "* Edgar Allen Poe\n" + 
    			 "* Sheri S. Tepper\n" + 
    			 "* Bill Bryson ";
    	 
    	 Boolean value = ConfigMethod.funcCreateForTest(contenu, titre);
    	 
    	 assertTrue(value);
         
         
         
         }
    
}
