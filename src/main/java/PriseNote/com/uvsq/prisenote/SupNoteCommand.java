/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package PriseNote.com.uvsq.prisenote;

import java.io.File;
import java.util.Scanner;

/**
 *
 * @author mayim
 */
public class SupNoteCommand implements I_Commande {

    private Note theNote;
    String titre;
    
    @Override
    public void execute() {
        

        System.out.println("Quel fichier desirez vous supprimé\n????");

        Scanner sc = new Scanner(System.in);
        String titre = sc.nextLine();

        File f = new File("src/main/java/PriseNote/com/uvsq/prisenote/adoc/ "+titre + ".adoc");
        File h = new File("src/main/java/PriseNote/com/uvsq/prisenote/adoc/ "+titre + ".html");
        if (f.exists()) {
            System.out.println("Le fichier existe");
           boolean test = f.delete();

            if (test == true) {
                System.out.println("Suppression du  fichier "+ titre+" OK");
                   
            } else {
                System.out.println("Suppression du  fichier "+ titre+" KO");
            }
        } else {
            System.out.println("Le fichier "+ titre+".adoc n'existe  pas");

        }
        if (h.exists()) {
            System.out.println("");
            boolean test2 = h.delete();

            if (test2 == true) {
                System.out.println("");
            } else {
                System.out.println("");
            }
        } else {
            System.out.println("");
        }
        I_Commande s = new GenerateIndexCommand();
                        s.execute();
    }
        
}
