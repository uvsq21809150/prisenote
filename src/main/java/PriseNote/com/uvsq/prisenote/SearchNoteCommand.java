/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package PriseNote.com.uvsq.prisenote;

import java.io.File;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.filefilter.DirectoryFileFilter;
import org.apache.commons.io.filefilter.TrueFileFilter;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Scanner;
import org.apache.commons.io.filefilter.RegexFileFilter;

/**
 *
 * @author mayim
 */
public class SearchNoteCommand implements I_Commande{
	private String search;
	
	
	private static String filePath ="";
    static int fileCount = 0;   
    static String fileName ;
    static int lineNumber=0;
    
    private static final String folderPath = "src/main/java/PriseNote/com/uvsq/prisenote/adoc/";

	public SearchNoteCommand(String search) {
	       // this.e = theNote;
			this.search=search;
	    }
	public void execute() {
		
		File dir = new File(folderPath);
        List<File> files =(List<File>) FileUtils.listFiles( dir, TrueFileFilter.INSTANCE, DirectoryFileFilter.DIRECTORY);
        for (File file : files) {
            try {
                System.out.println(file.getCanonicalPath()+ " est scanné."+"\n");
                filePath=file.getCanonicalPath();

                if (file.isFile()) {
                    
                    fileName=file.getName();
                    try {
                        FileReader reader = new FileReader(filePath );
                        BufferedReader br = new BufferedReader(reader); 
                        String s; 
                        while((s = br.readLine()) != null) { 
                        	lineNumber++;
                            if(s.contains(search)){  
                            	System.out.println("****************Resultat à ce niveau:*******************"+"\n");
                                System.out.println(search+" se trouve dans le fichier"+ file.getName()+ " après parcour de "+lineNumber+" ligne(s)\n"+ "----"+s.trim()+ "----"+"\n");
                                System.out.println("******************Fin à ce niveau**********************"+"\n\n\n\n");
                                } 
                        } 
                        reader.close(); 
                    }
                    catch(Exception e){
                        e.printStackTrace();
                    }
                    }

            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
		
	}
 
}
