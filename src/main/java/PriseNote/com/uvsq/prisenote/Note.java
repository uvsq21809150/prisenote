package PriseNote.com.uvsq.prisenote;
import java.io.Serializable;

import org.asciidoctor.ast.Author;

//Receiver

public class Note implements Serializable {

    private String titre;
    private String Author;
	private Object context;
    private Object project;

    public String getAuthor() {
		return Author;
	}

	public void setAuthor(String author) {
		Author = author;
	}
    
    public Note(String titre) {
        super();
        this.titre = titre;
    }

	public void setTitre(String titre) {
		this.titre = titre;
	}

	public String getTitre() {
		return titre;
	}

	public Object getContext() {
		return context;
	}

	public void setContext(Object context) {
		this.context = context;
	}

	public Object getProject() {
		return project;
	}

	public void setProject(Object project) {
		this.project = project;
	}
	
	

}
