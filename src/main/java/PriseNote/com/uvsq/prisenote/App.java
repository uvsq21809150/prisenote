package PriseNote.com.uvsq.prisenote;

import org.asciidoctor.Asciidoctor;
import org.apache.commons.lang.StringUtils;
import org.asciidoctor.*;
import static org.asciidoctor.Asciidoctor.Factory.create;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.nio.charset.StandardCharsets;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

/**
 * Hello world!
 *
 */
public class App {
	
	
	 public void display_menu() {        
         
         System.out.println("			======   ==    	=========   ============   ========	");
         System.out.println("			||  "+"\\"+"\\   " +"||     ||     ||	 ||	   ||			  		");
         System.out.println("			||  "+" \\"+"\\" +"  ||   	||     ||	 ||	   ||======		"		);
         System.out.println("			||  "+"  \\"+"\\"+" ||    	||     ||	 ||	   ||						");
         System.out.println("			==     ====     ========= 	 == 	   ========	");
         
         System.out.println("\n");
         System.out.println("			===================================================================");
         System.out.println("			|   MENU SELECTION         					  |");
         System.out.println("			===================================================================");
         System.out.println("			| Options:                 					");
         System.out.println("			|     1. Mode sans Argument					");
         System.out.println("			|     	*create*          	 pour creer un fichier		");
         System.out.println("			|    	*ls* 		         pour lister les fichiers		");
         System.out.println("			|     	*del ou delete*     	 pour supprimer un fichier		");
         System.out.println("			|    	*v ou view* 	    	 pour afficher un fichier		");
         System.out.println("			|    	*3* 	   		 pour sortir du programme		");
         System.out.println("			|     2. Mode avec Argument									");
         System.out.println("			|     	*e ou edit nomFile* 	 pour creer un fichier	");
         System.out.println("			|    	*ls* 		         pour lister les fichiers		");
         System.out.println("			|     	*del ou delete nomFile*  pour supprimer un fichier	");
         System.out.println("			|    	*v ou view nomFile* 	 pour afficher un fichier	");
         System.out.println("			|    	*3* 		   	 pour sortir du programme		");
         System.out.println("			|     3. Exit              					"   );
         System.out.println("			===================================================================");
         
         
         System.out.println("Veuillez choisir d'abord choisir votre mode ou appuyer sur 3 pour sortir\n");
         System.out.println("				En attente de votre commande..............\n");
		  }
	 
	 
	 public App() {
		    Scanner in = new Scanner ( System.in );
		    
		    display_menu();
		    switch ( in.nextInt() ) {
		      case 1:
		        System.out.println ( "Bienvenue dans le mode sans Argument" );
		        prog();
		        break;		        
		      case 2:
		        System.out.println ( "Bienvenue dans le mode avec Argument" );
		        prog();
		        break;
		      case 3:
		          System.out.println("Sortie du programme");
		          break;
		      default:
		        System.err.println ( "Option non reconnue" );
		        break;
		    }
		  }
	
	 public static void main ( String[] args ) {
		    new App();
		  }

    public  void prog() {
        while (true) {
        	System.out.println("\n************************** \n");
            System.out.println("*         Bienvenue        *");
            System.out.println("\n************************** \n");  
            System.out.println("En attente de votre commande..............");
            Scanner keyboard = new Scanner(System.in);
            String command;

            command = keyboard.nextLine();
            String[] comm1 = command.split(" ", 2);

            String test = command.substring(comm1[0].length(), command.length());
            //test=test.split(' ');
            //System.out.println("VALUE OF CUTTING IS: "+test);

            if (!test.isEmpty()) {
                System.out.println("Mode avec argument!\n l'argument est: " + test);
                /*Exécution de la commande avec argument*/
                switch (comm1[0]) {
                    case "create":
                        // code block
                        I_Commande a = new CreerNoteCommandArg(test);
                        a.execute();
                        break;
                    case "edit":
                        // code block
                        I_Commande enc = new EditNoteCommand(test);
                        enc.execute();
                        break;
                    case "e":
                        // code block
                        I_Commande ence = new EditNoteCommand(test);
                        ence.execute();
                        System.out.println("2nd parameter is: " + test);
                        break;
                    case "s":
                        // code block
                        I_Commande s = new SearchNoteCommand(test);
                        s.execute();
                        break;
                    case "search":
                        // code block
                        I_Commande se = new SearchNoteCommand(test);
                        se.execute();
                        break;

                    case "del":
                        I_Commande c = new SupNoteCommandArg(test);
                        c.execute();
                        break;
                    case "delete":
                        I_Commande b = new SupNoteCommandArg(test);
                        b.execute();
                        // code block
                        break;
                    case "view":
                        I_Commande ef = new VoirNoteCommandArg(test);
                        ef.execute();
                        // code block
                        break;
                    case "v":
                        I_Commande efa = new VoirNoteCommandArg(test);
                        efa.execute();
                        // code block
                        break;
                    case "lister":
                        I_Commande d = new ListerNoteCommand();
                        d.execute();
                        break;
                    // code block
                    case "ls":
                        I_Commande d2 = new ListerNoteCommand();
                        d2.execute();
                        break;
                    // code block
                    case "3":
      		          System.out.println("Sortie du programme");
      		          System.exit(0);
      		          break;
                    default:
                        System.out.println("Verifier la syntaxe de votre commande!!!!");
                }
                /*FIn ex�cution de la commande*/
                System.out.println("Mode avec argument!");

            } else {

                /*Execution de la commande sans argument*/
                switch (comm1[0]) {
                    case "create":
                        // code block
                        I_Commande a = new CreerNoteCommand();
                        a.execute();
                        break;														
                      case "edit":
                        // code block
                        I_Commande enc = new EditNoteCommand(test);
                        enc.execute();
                        System.out.println("2nd parameter is: " + test);
                        break;
                    case "e":
                        // code block
                        I_Commande ence = new EditNoteCommand(test);
                        ence.execute();
                        System.out.println("2nd parameter is: " + test);
                        break;
                     
                    case "delete":
                        I_Commande b = new SupNoteCommand();
                        b.execute();
                        // code block
                        break;
                    case "del":
                        I_Commande b2 = new SupNoteCommand();
                        b2.execute();
                        // code block
                        break;
                    case "view":
                        I_Commande c = new VoirNoteCommand();
                        c.execute();
                        // code block
                        break;
                    case "v":
                        I_Commande c2 = new VoirNoteCommand();
                        c2.execute();
                        // code block
                        break;
                    case "lister":
                        I_Commande d = new ListerNoteCommand();
                        d.execute();
                        // code block
                        break;
                    case "ls":
                        I_Commande d2 = new ListerNoteCommand();
                        d2.execute();
                        break;
                    case "3":
      		          System.out.println("Sortie du programme");
      		          System.exit(0);
      		          break;
                    default:
                        System.out.println("Verifier la syntaxe de votre commande!!!!");
                }
                /*FIn exécution de la commande*/

            }

        }

    }
}
