/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package PriseNote.com.uvsq.prisenote;

import java.awt.Desktop;
import java.io.File;
import java.io.IOException;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author mayim
 */
public class VoirNoteCommandArg implements I_Commande{
String titre;

    public VoirNoteCommandArg(String titre) {
        this.titre = titre;
    }

    @Override
    public void execute() {
       
        try {
            

            File h = new File("src/main/java/PriseNote/com/uvsq/prisenote/adoc/"+titre+".html");
            if (h.exists()) {
                System.out.println("Le fichier existe");
                Desktop.getDesktop().browse(h.toURI());
            } else {
                System.out.println("Le fichier n'existe pas");

            }

            //theNote.voir();
        } catch (IOException ex) {
            Logger.getLogger(VoirNoteCommand.class.getName()).log(Level.SEVERE, null, ex);
        }

    }
    }
    

