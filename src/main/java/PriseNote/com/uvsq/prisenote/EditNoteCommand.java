/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package PriseNote.com.uvsq.prisenote;

import Config.ConfigMethod;
import java.awt.Desktop;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Map;
import org.asciidoctor.Asciidoctor;
import static org.asciidoctor.Asciidoctor.Factory.create;
import org.asciidoctor.Attributes;
import static org.asciidoctor.AttributesBuilder.attributes;
import static org.asciidoctor.OptionsBuilder.options;

/**
 *
 * @author mayim
 */
public class EditNoteCommand implements I_Commande {
    //Editer e;

    private String titre;
    //File file= new File("");
    String chemin = /*file.getAbsolutePath()+*/ "src/main/java/PriseNote/com/uvsq/prisenote/adoc/";

    public EditNoteCommand(String titre) {
        // this.e = theNote;
        this.titre = titre;
    }

    public void execute() { 
        Asciidoctor asciidoctor = create();
        Map<String, Object> attributes = attributes().backend("html").asMap();
        Attributes attributess = attributes().attributes("context project").get();
        org.asciidoctor.Options optionss = options().attributes(attributes).get();
        File file = new File(chemin + titre + ".adoc");
        if (Desktop.isDesktopSupported()) {
            if (file.isDirectory()) {
                try {
                    throw new Exception("Expected file but found directory instead: " + file.getAbsolutePath());
                } catch (Exception e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }
            if (file.exists()) {
                try {
                    //Desktop.getDesktop().open(file);
                    ProcessBuilder p = new ProcessBuilder();
                    p.command(ConfigMethod.getEditorName(),chemin+titre+".adoc");
                    p.start();
                } catch (IOException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
                File file1 = new File(chemin + titre + ".html");
                file1.delete();
                asciidoctor.convertFile(file, optionss);
            } else {
                try {
                    file.createNewFile();
                    FileWriter fil = new FileWriter(file);
                    fil.write("=" + titre);
                    
                    //Ecriture automatique de la date dans le fichier lors de sa création
                        /* fil.write("\n");	
			            fil.write("Editer le ");
			            DateFormat df = new SimpleDateFormat("dd/MM/yyyy à HH:mm:ss");
			            java.util.Date today = Calendar.getInstance().getTime();
			            String reportDate = df.format(today);
			            String dateToPrintToFile = reportDate;
			            fil.write(dateToPrintToFile);*/

                    fil.close();
                    //Desktop.getDesktop().edit(file);
                    
                    ProcessBuilder p = new ProcessBuilder();
                    p.command(ConfigMethod.getEditorName(),chemin+titre+".adoc");
                    p.start();
                    
                    File file1 = new File(chemin + titre + ".html");
                    file1.delete();
                    asciidoctor.convertFile(file, optionss);
                } catch (IOException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

            }
        }

    }

}
