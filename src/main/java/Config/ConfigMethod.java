package Config;

import static org.asciidoctor.Asciidoctor.Factory.create;
import static org.asciidoctor.AttributesBuilder.attributes;
import static org.asciidoctor.OptionsBuilder.options;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.asciidoctor.Asciidoctor;
import org.asciidoctor.Attributes;

import PriseNote.com.uvsq.prisenote.Note;

public class ConfigMethod {

	public static List<Note> listDeNote = new ArrayList<Note>();
	
	public static String getAdocFilePath() throws IOException {
		BufferedReader brTest = new BufferedReader(new FileReader("src/main/java/Config/conf.txt"));
		String path = brTest .readLine();
		return path;
	  }
	
	public static String getEditorName() throws IOException {
		BufferedReader brTest = new BufferedReader(new FileReader("src/main/java/Config/conf.txt"));
		brTest .readLine();
		String path = brTest .readLine();
		return path;
	  }
	 public static Boolean funcCreateForTest(String contenuNote, String titre) {
		 Map<String, Object> attributes = attributes().backend("html").asMap();
		    
		    
		    Map<String, Object> attribute = new HashMap<String, Object>(){
		    	{
		    		put("context", null);
		    	    put("project", null);
		    }
		    
		    };
		    Map<String, Object> options = options().inPlace(true).attributes(attribute).asMap();
		            
		    Attributes attributess = attributes().attributes("context project").get();
		    org.asciidoctor.Options optionss = options().attributes(attributes).get();
	    	Asciidoctor asciidoctor = create();
	    	
	    	Boolean return_vale = false;
	    	
	    	//mesLines[0].substring(1, mesLines[0].length()) 
	        File file = null;
			try {
				file = new File(ConfigMethod.getAdocFilePath(),titre  + ".adoc");
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
	               
	        try (PrintWriter out = new PrintWriter(
	        		new OutputStreamWriter(new FileOutputStream(file), StandardCharsets.UTF_8)
	        		)) {
	            out.println(contenuNote);
	        } catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	        
	        asciidoctor.convertFile(file, optionss);
	        
	        return_vale = file.exists();
	        return return_vale;
	    }
}
